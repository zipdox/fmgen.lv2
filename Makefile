CC=gcc
OBJECT_CORE=fmgen.so
OBJECT_UI=fmgen_ui.so
BUILD_DIR=./build
OPTS=-Wall -O3 -fpic -lm
# OPTS=-g
LIBS=`pkg-config --libs gtk+-3.0 fftw3f`
FLAGS=`pkg-config --cflags gtk+-3.0 fftw3f`

SRCS_CORE=$(shell find ./src/core/*.c)
OBJS_CORE=$(patsubst ./src/%.c,$(BUILD_DIR)/%.o,$(SRCS_CORE))
SRCS_UI=$(shell find ./src/ui/*.c)
OBJS_UI=$(patsubst ./src/%.c,$(BUILD_DIR)/%.o,$(SRCS_UI))
RESOURCES=./src/ui/resources.xml
RESOURCES_SRC=$(BUILD_DIR)/ui/resources.c
RESOURCES_OBJ=$(BUILD_DIR)/ui/resources.o

all: $(OBJECT_CORE) $(OBJECT_UI)

$(RESOURCES_SRC): $(RESOURCES)
	glib-compile-resources --generate-source --target=$@ --sourcedir=$(dir $<) $^

$(RESOURCES_OBJ): $(RESOURCES_SRC)
	$(CC) -c -o $@ $^ $(OPTS) $(FLAGS)

$(OBJECT_CORE): $(OBJS_CORE)
	$(CC) -shared -o $@ $^ $(LIBS)

$(OBJECT_UI): $(OBJS_UI) $(RESOURCES_OBJ)
	$(CC) -shared -o $@ $^ $(LIBS)

$(OBJS_CORE): build/core/%.o: src/core/%.c
	mkdir -p $(dir $@)
	$(CC) $(OPTS) -c $< -o $@ $(FLAGS)

$(OBJS_UI): build/ui/%.o: src/ui/%.c
	mkdir -p $(dir $@)
	$(CC) $(OPTS) -c $< -o $@ $(FLAGS)

clean:
	rm -rf build/
	rm -f *.so
