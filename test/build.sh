#!/bin/sh
cd ..
git submodule update --init --recursive
cd test

cd redsea
./autogen.sh
./configure
make -j$(nproc)
cd ..

npm install
