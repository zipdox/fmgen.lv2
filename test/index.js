const blessed = require('blessed');
const { spawn } = require('node:child_process');

const screen = new blessed.screen({
    smartCSR: true,
    fullUnicode: true
});
screen.title = 'RDS Receiver';

const box = blessed.box({
    top: 'center',
    left: 'center',
    width: '80%',
    height: '80%',
    content: '',
    tags: true,
    border: {
      type: 'line'
    }
});
screen.append(box);

let ps = '';
let rt = '';

function update(){
    box.setContent(`{bold}{green-fg}${ps}{/green-fg}{/bold}\n${rt}`);
    screen.render();
}

const redsea = spawn('./fmgen.sh');
redsea.stdout.on('data', (data)=>{
    const json = JSON.parse(data.toString());
    switch(json.group){
        case '0B':
            if(!json.ps) break;
            ps = json.ps;
            break;
        case '2A':
            if(!json.radiotext) break;
            rt = json.radiotext;
            break;
    }
    update();
});
