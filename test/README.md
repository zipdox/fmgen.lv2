# Test FMGen

FMGen is tested using [redsea](https://github.com/windytan/redsea). This will be pulled and cloned by build.sh. I'm assuming you're already familiar with jack and have it running on your system for the test.

Install these packages first:
- automake, autoconf, compiler, etc.
- nodejs
- jackd
- carla (or other plugin host that creates JACK clients for plugins)
- jack-stdio
- libsndfile1-dev or libsndfile
- libliquid-dev or liquid-dsp

Then simply run `./build.sh`.

I'm assuming you've already built FMGen and added it to a path Carla scans. Make sure Carla's engine process mode is set to "Multiple clients". Load up FMGen into Carla and simply run `npm start`. Then you can use any media player that announces its track to the MPRIS D-Bus Interface to test the radio text.
