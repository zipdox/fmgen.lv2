# FMGen
LV2 plugin FM stereo encoder with RDS that works with the MPRIS D-Bus API.

## Building

Firstly install the dependencies. The package names might differe in different package managers.

### Dependencies
- libfftw3-dev
- lv2-dev
- libgtk-3-dev

Of course you will also need gcc and make. 

Simply run `make`.

## Installing
After building, Simply copy this directory into one of the directories your plugin host scans for LV2 plugins. Ideally you should have cloned the repo in there in the first place.

## Usage
This plugin does not perform any pre-emphasis or limiting, because it's desirable to have a high level of control over this process. You could use [Calf Emphasis](https://calf-studio-gear.org/#p_emphasis) with a limiter or compressor of choice for this. Simply connect these to the input of FMGen and that's all there is to it.
