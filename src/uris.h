#include <lv2/atom/atom.h>
#include <lv2/parameters/parameters.h>
#include <lv2/urid/urid.h>

#define FMGEN_URI "http://plugins.zipdox.net/fmgen"

typedef struct {
    LV2_URID atom_Vector;
    LV2_URID atom_Float;
    LV2_URID atom_Int;
    LV2_URID atom_eventTransfer;
    LV2_URID param_sampleRate;

    LV2_URID RawAudio;
    LV2_URID audioData;
    LV2_URID RadioText;
    LV2_URID text;
    LV2_URID ui_State;
    LV2_URID ui_On;
    LV2_URID ui_Off;
} FMGenURIs;

static inline void map_uris(LV2_URID_Map* map, FMGenURIs* uris){
    uris->atom_Vector = map->map(map->handle, LV2_ATOM__Vector);
    uris->atom_Float = map->map(map->handle, LV2_ATOM__Float);
    uris->atom_Int = map->map(map->handle, LV2_ATOM__Int);
    uris->atom_eventTransfer = map->map(map->handle, LV2_ATOM__eventTransfer);
    uris->param_sampleRate = map->map(map->handle, LV2_PARAMETERS__sampleRate);

    uris->RawAudio = map->map(map->handle, FMGEN_URI "#RawAudio");
    uris->audioData = map->map(map->handle, FMGEN_URI "#audioData");
    uris->RadioText = map->map(map->handle, FMGEN_URI "#RadioText");
    uris->text = map->map(map->handle, FMGEN_URI "#text");
    uris->ui_State = map->map(map->handle, FMGEN_URI "#UIState");
    uris->ui_On = map->map(map->handle, FMGEN_URI "#UIOn");
    uris->ui_Off = map->map(map->handle, FMGEN_URI "#UIOff");
}
