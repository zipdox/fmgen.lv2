#include <math.h>

double
ft_hannhamm (float* window, unsigned int n, double a, double b)
{
    double sum = 0.0;
    const double c = 2.0 * M_PI / (n - 1.0);
    for (int i = 0; i < n; ++i) {
        window[i] = a - b * cos (c * i);
        sum += window[i];
    }
    return sum;
}
