#include <lv2/ui/ui.h>
#include <lv2/core/lv2.h>
#include <lv2/atom/forge.h>
#include <gtk/gtk.h>
#include <fftw3.h>
#include <math.h>

#include "../uris.h"
#include "../constants.h"
#include "window.h"

#define GRAPH_X(width, x, samples) ((double)x / (samples - 1.0) * width)
#define GRAPH_Y(height, y) ((1.0 - y) * height)

#define BEZIER_1(start, end) (start + (end - start) / 3.0)
#define BEZIER_2(start, end) (start + (end - start) / 1.5)

typedef struct {
    LV2_URID_Map *map;
    LV2_Atom_Forge forge;
    FMGenURIs uris;
    LV2UI_Write_Function write;
    LV2UI_Controller controller;
    float sample_rate;

    int fft_buf_idx;
    float *fft_in;
    float *fft_out;
    fftwf_plan plan;
    unsigned int fft_out_size;
    float *window;
    float *power;

    GtkBuilder *builder;
    GtkWidget *box;
    GtkWidget *spectrum;
    // GtkWidget *window;

    GtkEntry *rt_entry;
    GtkButton *apply_button;
} FMGenGUI;

// static gboolean
// on_window_closed(GtkWidget* widget, GdkEvent* event, gpointer data)
// {
//   SamplerUI* ui = (SamplerUI*)data;

//   // Remove widget so Gtk doesn't delete it when the window is closed
//   gtk_container_remove(GTK_CONTAINER(ui->window), ui->box);
//   ui->window = NULL;

//   return FALSE;
// }


static inline float db_scale(float amp){
    float scaled = log10f(amp) / 9.0f + 1.0f;
    if(scaled < 0.0f) scaled = 0.0f;
    if(scaled > 1.0f) scaled = 1.0f;
    return scaled;
}

static void button_clicked(GtkButton *button, gpointer user_data){
    FMGenGUI *gui = user_data;
    g_print("%s\n", gtk_entry_get_text(GTK_ENTRY(gui->rt_entry)));
}

static void draw_curve(cairo_t *cr, guint width, guint height, float *amplitudes, unsigned int n_amplitudes){
    cairo_new_path(cr);
    cairo_move_to(cr, GRAPH_X(width, 0, n_amplitudes), GRAPH_Y(height, db_scale(amplitudes[0])));
    for(int i = 1; i < n_amplitudes; i++){
        cairo_line_to(cr, GRAPH_X(width, i, n_amplitudes), GRAPH_Y(height, db_scale(amplitudes[i])));
    }
}

static void draw_vertical_line(cairo_t *cr, guint width, guint height, double x){
    cairo_new_path(cr);
    cairo_move_to(cr, x * width, 0.0);
    cairo_line_to(cr, x * width, height);
    cairo_stroke(cr);
}

static void draw_horizontal_line(cairo_t *cr, guint width, guint height, double y){
    cairo_new_path(cr);
    cairo_move_to(cr, 0, y * height);
    cairo_line_to(cr, width, y * height);
    cairo_stroke(cr);
}

static void draw_text_top(cairo_t *cr, double x, double y, char *text){
    cairo_text_extents_t extents;
    cairo_text_extents(cr, text, &extents);
    cairo_move_to(cr, x - extents.width / 2.0, y + extents.height);
    cairo_show_text(cr, text);
}

#define FREQ_PILOT_S "19k"
#define FREQ_STEREO_S "38k"
#define FREQ_RDS_S "57k"

static gboolean draw_callback(GtkWidget *widget, cairo_t *cr, gpointer user_data){
    FMGenGUI *gui = user_data;

    guint width = gtk_widget_get_allocated_width (widget);
    guint height = gtk_widget_get_allocated_height (widget);

    cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
    cairo_rectangle(cr, 0.0, 0.0, width, height);
    cairo_fill(cr);

    if(gui->sample_rate < MIN_SAMPLE_RATE){
        cairo_set_source_rgb(cr, 1.0, 0.0, 0.0);
        cairo_set_font_size(cr, 32);
        cairo_move_to(cr, 16, 48);
        cairo_show_text(cr, "Sample rate too low!");
        cairo_stroke(cr);
        return FALSE;
    }

    double freq_edge = gui->sample_rate / 3.0;
    cairo_set_source_rgb(cr, 0.2, 0.2, 0.2);
    draw_vertical_line(cr, width, height, FREQ_PILOT / freq_edge);
    draw_vertical_line(cr, width, height, FREQ_STEREO / freq_edge);
    draw_vertical_line(cr, width, height, FREQ_RDS / freq_edge);
    for(int i = 10; i <= 80; i += 10){
        draw_horizontal_line(cr, width, height, i/90.0);
    }

    cairo_set_line_width(cr, 1);
    cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 0.5);
    draw_curve(cr, width, height, gui->power, gui->fft_out_size / 3);
    cairo_line_to(cr, width, height);
    cairo_line_to(cr, 0.0, height);
    cairo_close_path(cr);
    cairo_fill(cr);

    cairo_set_source_rgb(cr, 0.0, 1.0, 0.0);
    draw_curve(cr, width, height, gui->power, gui->fft_out_size / 3);
    cairo_stroke(cr);

    cairo_set_source_rgb(cr, 1.0, 1.0, 1.0);
    cairo_set_font_size(cr, 16);
    draw_text_top(cr, FREQ_PILOT / freq_edge * width, 8.0, FREQ_PILOT_S);
    draw_text_top(cr, FREQ_STEREO / freq_edge * width, 8.0, FREQ_STEREO_S);
    draw_text_top(cr, FREQ_RDS / freq_edge * width, 8.0, FREQ_RDS_S);

    return FALSE;
}

static void
send_ui_enable(FMGenGUI *gui)
{
    uint8_t obj_buf[64];
    lv2_atom_forge_set_buffer(&gui->forge, obj_buf, sizeof(obj_buf));

    LV2_Atom_Forge_Frame frame;
    LV2_Atom *msg = (LV2_Atom*)lv2_atom_forge_object(&gui->forge, &frame, 0, gui->uris.ui_On);

    assert(msg);

    lv2_atom_forge_pop(&gui->forge, &frame);
    gui->write(
        gui->controller,
        0,
        lv2_atom_total_size(msg),
        gui->uris.atom_eventTransfer,
        msg
    );
}

static void
send_ui_disable(FMGenGUI *gui)
{
//   send_ui_state(gui);

    uint8_t obj_buf[64];
    lv2_atom_forge_set_buffer(&gui->forge, obj_buf, sizeof(obj_buf));

    LV2_Atom_Forge_Frame frame;
    LV2_Atom *msg = (LV2_Atom*)lv2_atom_forge_object(&gui->forge, &frame, 0, gui->uris.ui_Off);

    assert(msg);

    lv2_atom_forge_pop(&gui->forge, &frame);
    gui->write(
        gui->controller,
        0,
        lv2_atom_total_size(msg),
        gui->uris.atom_eventTransfer,
        msg
    );
}

static LV2UI_Handle
instantiate(const LV2UI_Descriptor*   descriptor,
            const char*               plugin_uri,
            const char*               bundle_path,
            LV2UI_Write_Function      write_function,
            LV2UI_Controller          controller,
            LV2UI_Widget*             widget,
            const LV2_Feature* const* features)
{
    FMGenGUI *gui = g_new0(FMGenGUI, 1);

    gui->write = write_function;
    gui->controller = controller;
    for (int i = 0; features[i]; ++i) {
        if (!strcmp(features[i]->URI, LV2_URID_URI "#map")) {
            gui->map = (LV2_URID_Map*)features[i]->data;
        }
    }

    map_uris(gui->map, &gui->uris);
    lv2_atom_forge_init(&gui->forge, gui->map);

    gui->builder = gtk_builder_new_from_resource("/ui.glade");
    gui->box = GTK_WIDGET(gtk_builder_get_object(gui->builder, "fmgen_box"));

    gui->rt_entry = GTK_ENTRY(gtk_builder_get_object(gui->builder, "rt_entry"));
    gui->apply_button = GTK_BUTTON(gtk_builder_get_object(gui->builder, "apply_button"));
    g_signal_connect(G_OBJECT(gui->apply_button), "clicked", G_CALLBACK(button_clicked), gui);
    gui->spectrum = GTK_WIDGET(gtk_builder_get_object(gui->builder, "spectrum"));
    g_signal_connect(G_OBJECT(gui->spectrum), "draw", G_CALLBACK (draw_callback), gui);

    *widget = gui->box;

    gui->fft_in = fftwf_alloc_real(FFT_SIZE);
    gui->fft_out_size = FFT_SIZE;
    gui->fft_out = fftwf_alloc_real(FFT_SIZE);
    gui->plan = fftwf_plan_r2r_1d(FFT_SIZE, gui->fft_in, gui->fft_out, FFTW_R2HC, FFTW_MEASURE);

    gui->window = g_new0(float, FFT_SIZE);
    double sum = ft_hannhamm(gui->window, FFT_SIZE, 0.5, 0.5);
    const double isum = 2.0 / sum;
    for (unsigned int i = 0; i < FFT_SIZE; i++) {
        gui->window[i] *= isum;
    }
    gui->power = g_new0(float, FFT_SIZE);

    send_ui_enable(gui);

    return gui;
}

static void
cleanup(LV2UI_Handle handle)
{
    FMGenGUI *gui = handle;
    send_ui_disable(gui);

    fftwf_free(gui->fft_in);
    fftwf_free(gui->fft_out);
    fftwf_destroy_plan(gui->plan);
    g_free(gui->window);

    // gtk_widget_destroy(gui->box);
    g_object_unref(gui->builder);
    g_free(gui);
}

static int
recv_raw_audio(FMGenGUI *gui, const LV2_Atom_Object* obj)
{
    const LV2_Atom* data_val = NULL;
    const int       n_props  = lv2_atom_object_get(
        obj, gui->uris.audioData, &data_val, NULL);

    if (n_props != 1 || data_val->type != gui->uris.atom_Vector) {
        // Object does not have the required properties with correct types
        return 1;
    }

    // Get the values we need from the body of the property value atoms
    const LV2_Atom_Vector* vec = (const LV2_Atom_Vector*)data_val;
    if (vec->body.child_type != gui->uris.atom_Float) {
        return 1; // Vector has incorrect element type
    }

    // Number of elements = (total size - header size) / element size
    const size_t n_elem = ((data_val->size - sizeof(LV2_Atom_Vector_Body)) / sizeof(float));

    // Float elements immediately follow the vector body header
    const float* data = (const float*)(&vec->body + 1);

    for(size_t i = 0; i < n_elem; i++){
        gui->fft_in[gui->fft_buf_idx] = data[i] * gui->window[gui->fft_buf_idx];
        if(++gui->fft_buf_idx >= FFT_SIZE){
            gui->fft_buf_idx = 0;
            fftwf_execute(gui->plan);
            gui->power[0] = gui->fft_out[0] * gui->fft_out[0];
            for (uint32_t i = 1; i < gui->fft_out_size - 1; ++i) {
                gui->power[i] = (gui->fft_out[i] * gui->fft_out[i]) + (gui->fft_out[FFT_SIZE - i] * gui->fft_out[FFT_SIZE - i]);
            }
            gtk_widget_queue_draw(gui->spectrum);
        }
    }

  return 0;
}

static int
recv_ui_state(FMGenGUI *gui, const LV2_Atom_Object* obj)
{
    const LV2_Atom* rate_val = NULL;

    const int n_props = lv2_atom_object_get(obj,
                                            gui->uris.param_sampleRate,
                                            &rate_val,
                                            NULL);

    if (n_props != 1 || rate_val->type != gui->uris.atom_Float) {
        // Object does not have the required properties with correct types
        // fprintf(stderr, "eg-scope.lv2 UI error: Corrupt state message\n");
        return 1;
    }

    // Get the values we need from the body of the property value atoms
    const float rate = ((const LV2_Atom_Float*)rate_val)->body;

    // Disable transmission and update UI
    gui->sample_rate = rate;

    return 0;
}

static void
port_event(LV2UI_Handle handle,
           uint32_t     port_index,
           uint32_t     buffer_size,
           uint32_t     format,
           const void*  buffer)
{
    FMGenGUI *gui = handle;
    const LV2_Atom* atom = (const LV2_Atom*)buffer;

    if(format == gui->uris.atom_eventTransfer && lv2_atom_forge_is_object_type(&gui->forge, atom->type)){
    const LV2_Atom_Object* obj = (const LV2_Atom_Object*)atom;
    if (obj->body.otype == gui->uris.RawAudio){
        recv_raw_audio(gui, obj);
    } else if (obj->body.otype == gui->uris.ui_State){
        recv_ui_state(gui, obj);
    }
  }
}

// static int
// ui_show(LV2UI_Handle handle)
// {

// }

// static int
// ui_hide(LV2UI_Handle handle)
// {

// }

static int
ui_idle(LV2UI_Handle handle)
{

    // FMGenGUI *gui = handle;
    // if(gui->window)
    g_print("run!\n");
        gtk_main_iteration_do(FALSE);
    return 0;
}

static const void*
extension_data(const char* uri)
{
//   static const LV2UI_Show_Interface show = {ui_show, ui_hide};
  static const LV2UI_Idle_Interface idle = {ui_idle};

//   if (!strcmp(uri, LV2_UI__showInterface)) {
//     return &show;
//   }

    if (!strcmp(uri, LV2_UI__idleInterface)) {
        return &idle;
    }

  return NULL;
}

static const LV2UI_Descriptor descriptor = {FMGEN_URI "#ui",
                                            instantiate,
                                            cleanup,
                                            port_event,
                                            // NULL};
                                            extension_data};

LV2_SYMBOL_EXPORT
const LV2UI_Descriptor*
lv2ui_descriptor(uint32_t index)
{
  return index == 0 ? &descriptor : NULL;
}
