/*
 *  FMGen
 *  Copyright (C) 2023  Zipdox
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <lv2/core/lv2.h>
#include <lv2/atom/forge.h>
#include <lv2/core/lv2_util.h>

#include <stdint.h>
#include <stdlib.h>
#include <math.h>

#include "firfilter.h"
#include "rrcosfilter.h"
#include "rds.h"
#include "mpris.h"
#include "../uris.h"
#include "../constants.h"

#define RDS_CUTOFF 1187.5

typedef enum {
    CONTROL_PORT,
    NOTIFY_PORT,
    INPUT_L,
    INPUT_R,
    OUTPUT,
    AUDIO_MOD,
    PILOT_MOD,
    RDS_MOD
} PortIndex;

typedef struct {
    const LV2_Atom_Sequence *control;
    LV2_Atom_Sequence *notify;
    LV2_URID_Map *map;
    LV2_Atom_Forge forge;
    LV2_Atom_Forge_Frame frame;
    FMGenURIs uris;

    double rate;
    gboolean ui_active;
    gboolean send_settings_to_ui;

    double t;
    double pilot_period;
    double t_sample;

    const float* input_l;
    const float* input_r;
    float*       output;

    FIRFilter *filter_l;
    FIRFilter *filter_r;

    char rds_cycle;
    char rds_bit_stage;
    char rds_data_bit;
    float rds_pulse;
    RRCosFilter *rds_filter;

    const float *audio_mod;
    const float *pilot_mod;
    const float *rds_mod;

    MPRISWatcher *watcher;
    GMainLoop *watcher_loop;
    GThread *watcher_thread;
    gboolean overwrite_rt;

    RDSContext *rds_ctx;
} FMGen;

static void mpris_cb(const gchar *title, gpointer cb_data){
    FMGen *fmgen = cb_data;
    g_print("%s\n", title);
    if(fmgen->overwrite_rt) RDSContext_set_rt(fmgen->rds_ctx, (char*)title);
}

static gpointer thread_func(gpointer user_data){
    FMGen *fmgen = user_data;
    GMainContext *context = g_main_context_new();
    g_main_context_push_thread_default(context);
    fmgen->watcher_loop = g_main_loop_new(context, FALSE);
    fmgen->watcher = MPRISWatcher_create(mpris_cb, NULL, user_data);
    g_main_loop_run(fmgen->watcher_loop);
    if(fmgen->watcher) MPRISWatcher_destroy(fmgen->watcher);
    g_main_loop_unref(fmgen->watcher_loop);
    g_main_context_unref(context);
    return NULL;
}

static LV2_Handle
instantiate(const LV2_Descriptor*     descriptor,
            double                    rate,
            const char*               bundle_path,
            const LV2_Feature* const* features)
{
    FMGen* fmgen = (FMGen*)calloc(1, sizeof(FMGen));

    lv2_features_query(
        features,
        LV2_URID__map, &fmgen->map, true,
        NULL
    );

    map_uris(fmgen->map, &fmgen->uris);
    lv2_atom_forge_init(&fmgen->forge, fmgen->map);

    fmgen->rate = rate;
    
    fmgen->pilot_period = 1.0 / FREQ_PILOT;
    fmgen->t_sample = 1.0 / rate;

    fmgen->filter_l = FIRFilter_create(rate, AUDIO_CUTOFF, AUDIO_FILTER_TRANSITION);
    fmgen->filter_r = FIRFilter_create(rate, AUDIO_CUTOFF, AUDIO_FILTER_TRANSITION);

    fmgen->rds_filter = RRCosFilter_create(rate, 1.0 / (2.0 * RDS_CUTOFF), RDS_CUTOFF / 2.0);

    fmgen->rds_ctx = RDSContext_create();
    fmgen->rds_ctx->pi = 0x80EF;
    fmgen->rds_ctx->pty = 11;
    RDSContext_set_ps(fmgen->rds_ctx, "Radio 69");
    fmgen->rds_ctx->di = 0b0001;

    fmgen->overwrite_rt = TRUE;
    fmgen->watcher_thread = g_thread_new("mpris_watcher", thread_func, fmgen);

    return (LV2_Handle)fmgen;
}

static void
connect_port(LV2_Handle instance, uint32_t port, void* data)
{
    FMGen* fmgen = (FMGen*)instance;

    switch ((PortIndex)port) {
        case INPUT_L:
            fmgen->input_l = (const float*)data;
            break;
        case INPUT_R:
            fmgen->input_r = (const float*)data;
            break;
        case OUTPUT:
            fmgen->output = (float*)data;
            break;
        case AUDIO_MOD:
            fmgen->audio_mod = (const float*)data;
            break;
        case PILOT_MOD:
            fmgen->pilot_mod = (const float*)data;
            break;
        case RDS_MOD:
            fmgen->rds_mod = (const float*)data;
            break;
        case CONTROL_PORT:
            fmgen->control = (const LV2_Atom_Sequence*)data;
            break;
        case NOTIFY_PORT:
            fmgen->notify = (LV2_Atom_Sequence*)data;
            break;
    }
}

static void
tx_rawaudio(LV2_Atom_Forge* forge,
            FMGenURIs*      uris,
            const size_t    n_samples,
            const float*    data)
{
  LV2_Atom_Forge_Frame frame;

  // Forge container object of type 'RawAudio'
  lv2_atom_forge_frame_time(forge, 0);
  lv2_atom_forge_object(forge, &frame, 0, uris->RawAudio);

  // Add vector of floats 'audioData' property
  lv2_atom_forge_key(forge, uris->audioData);
  lv2_atom_forge_vector(
    forge, sizeof(float), uris->atom_Float, n_samples, data);

  // Close off object
  lv2_atom_forge_pop(forge, &frame);
}

static void
run(LV2_Handle instance, uint32_t n_samples)
{
    FMGen* fmgen = (FMGen*)instance;

    // Process incoming events from GUI
    if(fmgen->control) {
        const LV2_Atom_Event* ev = lv2_atom_sequence_begin(&(fmgen->control)->body);
        // For each incoming message...
        while(!lv2_atom_sequence_is_end(&fmgen->control->body, fmgen->control->atom.size, ev)){
            // If the event is an atom:Blank object
            if(lv2_atom_forge_is_object_type(&fmgen->forge, ev->body.type)) {
                const LV2_Atom_Object* obj = (const LV2_Atom_Object*)&ev->body;
                if (obj->body.otype == fmgen->uris.ui_On) {
                    // If the object is a ui-on, the UI was activated
                    fmgen->ui_active = true;
                    fmgen->send_settings_to_ui = true;
                } else if (obj->body.otype == fmgen->uris.ui_Off) {
                    // If the object is a ui-off, the UI was closed
                    fmgen->ui_active = false;
                }
            }
            ev = lv2_atom_sequence_next(ev);
        }
    }

    // Prepare forge buffer and initialize atom-sequence
    lv2_atom_forge_set_buffer(&fmgen->forge, (uint8_t*)fmgen->notify, fmgen->notify->atom.size);
    lv2_atom_forge_sequence_head(&fmgen->forge, &fmgen->frame, 0);
    // Send settings to UI
    if (fmgen->send_settings_to_ui && fmgen->ui_active) {
        fmgen->send_settings_to_ui = false;
        // Forge container object of type 'ui_state'
        LV2_Atom_Forge_Frame frame;
        lv2_atom_forge_frame_time(&fmgen->forge, 0);
        lv2_atom_forge_object(&fmgen->forge, &frame, 0, fmgen->uris.ui_State);

        // Add UI state as properties
        lv2_atom_forge_key(&fmgen->forge, fmgen->uris.param_sampleRate);
        lv2_atom_forge_float(&fmgen->forge, (float)fmgen->rate);
        lv2_atom_forge_pop(&fmgen->forge, &frame);
    }

    if(fmgen->rate < MIN_SAMPLE_RATE) return;

    const float* const input_l = fmgen->input_l;
    const float* const input_r = fmgen->input_r;
    float* const output = fmgen->output;
    const float audio_mod = *(fmgen->audio_mod) / 100.0;
    const float pilot_mod = *(fmgen->pilot_mod) / 100.0;
    const float rds_mod = *(fmgen->rds_mod) / 100.0;

    float left, right;
    float audio, pilot, rds;

    for (uint32_t i = 0; i < n_samples; i++, fmgen->t += fmgen->t_sample) {
        if(fmgen->t >= fmgen->pilot_period) // reset the time to prevent loss of floating point accuracy
        {
            fmgen->t -= fmgen->pilot_period;
            if(++fmgen->rds_cycle >= 8){ // two times per RDS bit
                fmgen->rds_cycle = 0;

                if(fmgen->rds_bit_stage){
                    // inverse bit, manchester coding
                    fmgen->rds_pulse = fmgen->rds_data_bit ? -1.0 : 1.0;
                }else{
                    // differential encoding
                    fmgen->rds_data_bit ^= RDSContext_get_bit(fmgen->rds_ctx);
                    fmgen->rds_pulse = fmgen->rds_data_bit ? 1.0 : -1.0;
                }
                fmgen->rds_bit_stage ^= 1;
            }
            if(fmgen->rds_cycle >= 4){
                fmgen->rds_pulse = 0.0;
            }
        }

        left = FIRFilter_filter(fmgen->filter_l, input_l[i]);
        right = FIRFilter_filter(fmgen->filter_r, input_r[i]);

        audio = audio_mod * ((left + right) / 2.0 + (left - right) / 2.0 * sinf(4.0 * M_PI * FREQ_PILOT * fmgen->t));
        pilot = pilot_mod * sinf(2.0 * M_PI * FREQ_PILOT * fmgen->t);
        rds = RRCosFilter_filter(fmgen->rds_filter, (float)fmgen->rds_pulse) * rds_mod * sinf(6.0 * M_PI * FREQ_PILOT * fmgen->t);
        output[i] = audio + pilot + rds;

    }

    if(fmgen->ui_active)
        tx_rawaudio(&fmgen->forge, &fmgen->uris, n_samples, output);
}

static void
cleanup(LV2_Handle instance)
{
    FMGen* fmgen = (FMGen*)instance;
    FIRFilter_destroy(fmgen->filter_l);
    FIRFilter_destroy(fmgen->filter_r);
    RRCosFilter_destroy(fmgen->rds_filter);
    RDSContext_destroy(fmgen->rds_ctx);
    g_main_loop_quit(fmgen->watcher_loop);
    g_thread_join(fmgen->watcher_thread);
    free(instance);
}

static const void*
extension_data(const char* uri)
{
    return NULL;
}

static const LV2_Descriptor descriptor = {FMGEN_URI,
                                          instantiate,
                                          connect_port,
                                          NULL,
                                          run,
                                          NULL,
                                          cleanup,
                                          extension_data};

LV2_SYMBOL_EXPORT
const LV2_Descriptor*
lv2_descriptor(uint32_t index)
{
    return index == 0 ? &descriptor : NULL;
}
