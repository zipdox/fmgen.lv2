#include <math.h>
#include <stdlib.h>

#include "firfilter.h"

static inline float sincf(float x){
    if(x == 0.0f) return 1.0;
    return sinf(M_PI * x) / (M_PI * x);
}

static inline float *firwin(int taps, float cutoff, float sample_rate){
    cutoff = cutoff / (sample_rate * 0.5);

    float *h = calloc(taps, sizeof(float));

    float alpha = 0.5 * (taps * 2 - 1);
    float sum = 0.0;
    for(int i = 0; i < taps; i++){
        // calculate filter coefficients
        h[i] = cutoff * sincf(cutoff * ((float)i - alpha));
        // multiply by hamming window
        h[i] *= 0.54 + 0.46 * cosf((2.0 * i / (taps*2 - 1) - 1.0) * M_PI);
        // accumulate for scaling
        sum += h[i];
    }

    // scale
    sum *= 2.0;
    for(int i = 0; i < taps; i++){
        h[i] /= sum;
    }

    return h;
}

static inline int ntaps(float sample_rate, float transition_width){
    int ntaps = (53.0 * sample_rate / (22.0 * transition_width));
    if (ntaps & 1) ntaps++;
    return ntaps;
}

FIRFilter *FIRFilter_create(float sample_rate, float cutoff, float transition_width){
    FIRFilter *filter = calloc(1, sizeof(FIRFilter));
    filter->taps = ntaps(sample_rate, transition_width) / 2;
    filter->bufsize = filter->taps * 2;
    filter->h = firwin(filter->taps, cutoff, sample_rate);
    filter->buf = calloc(filter->bufsize, sizeof(float));
    return filter;
}

float FIRFilter_filter(FIRFilter *filter, float value){
    if(++filter->head >= filter->bufsize) filter->head = 0;
    filter->buf[filter->head] = value;
    float filtered = 0.0;
    for(int i = 0, j = filter->head + 1, k = filter->head; i < filter->taps; i++, j++, k--){
        if(j >= filter->bufsize) j = 0;
        if(k < 0) k = filter->bufsize - 1;
        filtered += filter->h[i] * (filter->buf[j] + filter->buf[k]);
    }
    return filtered;
}

void FIRFilter_destroy(FIRFilter *filter){
    free(filter->h);
    free(filter->buf);
    free(filter);
}
