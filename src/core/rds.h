#include <stdint.h>

#define PS_LEN 8
#define RT_LEN 64

typedef struct {
    uint16_t info;
    uint16_t check;
} RDSBlock;

typedef struct {
    uint16_t pi; // programme identification
    uint8_t ps[PS_LEN]; // programme service name
    uint8_t pty; // programme type
    uint8_t di, ta, ms; // decoder identification, traffic announcement, music/speech
    char rt[RT_LEN]; // radio text
    uint8_t rt_buf[RT_LEN];
    uint8_t rt_clear;
    uint8_t rt_groups;

    RDSBlock group[4];

    uint8_t message_idx; // what type of message
    uint8_t n_groups; // how many groups in the current message
    uint8_t group_idx; // what group of a message
    uint8_t block_idx; // what block of a group
    uint8_t bit_idx; // what bit of a block
} RDSContext;

RDSContext *RDSContext_create();

void RDSContext_set_ps(RDSContext *context, char *ps);

void RDSContext_set_rt(RDSContext *context, char *rt);

uint8_t RDSContext_get_bit(RDSContext *context);

void RDSContext_destroy(RDSContext *context);
