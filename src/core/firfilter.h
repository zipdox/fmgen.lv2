typedef struct {
    float *h;
    int taps;
    int bufsize;
    float *buf;
    int head;
} FIRFilter;

FIRFilter *FIRFilter_create(float sample_rate, float cutoff, float transition_width);

float FIRFilter_filter(FIRFilter *filter, float value);

void FIRFilter_destroy(FIRFilter *filter);
