#include <gio/gio.h>

typedef void (*PRISWatcher_cb)(const gchar *title, gpointer cb_data);

typedef struct {
    PRISWatcher_cb cb;
    gpointer cb_data;
    GDBusConnection *conn;
    GCancellable *conn_cancellable;
    guint sub_id;
    gchar *path;
} MPRISWatcher;

MPRISWatcher *MPRISWatcher_create(PRISWatcher_cb callback, gchar *path, gpointer cb_data);

void MPRISWatcher_watch(MPRISWatcher *watcher, gchar *path);

void MPRISWatcher_destroy(MPRISWatcher *watcher);
