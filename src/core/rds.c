#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "rds.h"

static inline uint8_t u8_get_bit(uint8_t value, uint8_t bit){
    return (value >> (7 - bit) & 1);
}

static inline uint8_t u16_get_bit(uint16_t value, uint8_t bit){
    return (value >> (15 - bit) & 1);
}

#define BLOCK_LEN 26
#define GROUP_LEN 4

static uint8_t RDSBlock_get_bit(RDSBlock *block, uint8_t idx){
    if(idx >= 16){
        return u16_get_bit(block->check, idx - 16);
    }else{
        return u16_get_bit(block->info, idx);
    }
}

static const uint16_t offset_words[] = {
    0b0011111100, // A
    0b0110011000, // B
    0b0101101000, // C
    0b0110110100, // D
    0b1101010000  // C'
};

static void RDSBlock_crc(RDSBlock *block, uint16_t offset){
    uint16_t crc = 0;

    for (int j = 0; j < 16; j++) {
        uint8_t bit = u16_get_bit(block->info, j);

        uint8_t msb = (crc >> 9) & 1;
        crc <<= 1;
        if ((msb ^ bit) != 0) crc ^= 0x1B9;
    }
    crc ^= offset;
    block->check = crc << 6;
}

static void RDSContext_crc_group(RDSContext *context){
    for(uint8_t i = 0; i < GROUP_LEN; i++){
        uint8_t offset = i;
        if(offset == 2 && ((context->group[1].info >> 11) & 1)) offset = 4;
        RDSBlock_crc(&context->group[i], offset_words[offset]);
    }
}

static uint16_t block2(uint8_t group_type, uint8_t b, uint8_t tp, uint8_t pty, uint8_t app){
    uint16_t block = 0;
    block |= group_type << 12;
    block |= (b & 1) << 11;
    block |= (tp & 1) << 10;
    block |= (pty & 0b11111) << 5;
    block |= (app & 0b11111);
    return block;
}

static void RDSContext_generate_station_name_group(RDSContext *context){
    context->n_groups = 4;
    context->group[2].info = context->pi;
    uint8_t app = context->group_idx;
    app |= u8_get_bit(context->di, context->group_idx + 4) << 2;
    app |= (context->ms & 1) << 3;
    app |= (context->ta & 1) << 4;
    context->group[1].info = block2(0, 1, 0, context->pty, app);
    uint8_t ps_idx = context->group_idx * 2; // 2 characters per group
    context->group[3].info = (context->ps[ps_idx] << 8) | context->ps[ps_idx + 1]; 
}

static void RDSContext_generate_radio_text_group(RDSContext *context){
    uint8_t clear_bit = 0;
    if(context->group_idx == 0){
        if(context->rt_clear){
            context->rt_clear = 0;
            memcpy(context->rt_buf, context->rt, 64);
            clear_bit = 1;
            unsigned long len = strlen((char*) context->rt_buf);
            if(len < RT_LEN){
                context->rt_buf[len] = '\r';
                len++;
            }
            context->rt_groups = len / 4;
            if(len % 4 > 0) context->rt_groups++;
        }
        context->n_groups = context->rt_groups;
    }
    context->group[1].info = block2(2, 0, 0, context->pty, context->group_idx | (clear_bit << 4));
    uint8_t rt_idx = context->group_idx * 4; // 4 characters per type A group
    context->group[2].info = (context->rt_buf[rt_idx] << 8) | context->rt_buf[rt_idx + 1]; 
    context->group[3].info = (context->rt_buf[rt_idx + 2] << 8) | context->rt_buf[rt_idx + 3];
}

typedef enum {
    RDS_MESSAGE_STATION_NAME,
    RDS_MESSAGE_RADIO_TEXT,
    RDS_MESSAGE_TYPES
} RDSMessage;

static void RDSContext_generate_group(RDSContext *context){
    if(++context->group_idx >= context->n_groups){
        context->group_idx = 0;
        // next message
        if(++context->message_idx >= RDS_MESSAGE_TYPES){
            context->message_idx = 0;
            // back to first message type
        }
    }
    context->group[0].info = context->pi;
    switch(context->message_idx){
        case RDS_MESSAGE_STATION_NAME:
            RDSContext_generate_station_name_group(context);
            break;
        case RDS_MESSAGE_RADIO_TEXT:
            RDSContext_generate_radio_text_group(context);
            break;
    }
    RDSContext_crc_group(context);
}

RDSContext *RDSContext_create(){
    RDSContext *context = calloc(1, sizeof(RDSContext));
    return context;
}

void RDSContext_set_ps(RDSContext *context, char *ps){
    memset(context->ps, 0, PS_LEN);
    unsigned long len = strlen(ps);
    memcpy(context->ps, ps, len > PS_LEN ? PS_LEN : len);
}

void RDSContext_set_rt(RDSContext *context, char *rt){
    memset(context->rt, 0, RT_LEN);
    unsigned long len = strlen(rt);
    memcpy(context->rt, rt, len > RT_LEN ? RT_LEN : len);
    context->rt_clear = 1;
}

uint8_t RDSContext_get_bit(RDSContext *context){
    if(++context->bit_idx >= BLOCK_LEN){
        context->bit_idx = 0;
        // next block
        if(++context->block_idx >= GROUP_LEN){
            context->block_idx = 0;
            // next group
            RDSContext_generate_group(context);
        }
    }
    return RDSBlock_get_bit(&context->group[context->block_idx], context->bit_idx);
}

void RDSContext_destroy(RDSContext *context){
    free(context);
}
