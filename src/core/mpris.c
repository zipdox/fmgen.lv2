#include "mpris.h"
#include "ebu.h"

#define OBJ_PATH "/org/mpris/MediaPlayer2"
#define PROPERTIES_VARIANT_TYPE "(sa{sv}as)"

static gchar *make_artist_string(GVariant *artists){
    GVariantIter *iter = g_variant_iter_new(artists);
    gsize children = g_variant_iter_n_children(iter);
    if(children == 0){
        g_variant_iter_free(iter);
        return NULL;
    }

    gchar **ptrs = g_new0(char*, children + 1);

    gint i = 0;
    while(g_variant_iter_loop(iter, "s", &ptrs[i])){
        i++;
    }
    g_variant_iter_free(iter);

    gchar *joined = g_strjoinv(", ", ptrs);

    g_free(ptrs);

    return joined;
}

static void signal_cb(
    GDBusConnection *connection,
    const gchar *sender_name,
    const gchar *object_path,
    const gchar *interface_name,
    const gchar *signal_name,
    GVariant *parameters,
    gpointer user_data
){
    if(g_strcmp0(g_variant_get_type_string(parameters), PROPERTIES_VARIANT_TYPE) != 0) return;

    MPRISWatcher *watcher = user_data;

    gchar *interface;
    GVariantIter *changed_properties;
    GVariantIter *invalidated_properties;

    g_variant_get(parameters, PROPERTIES_VARIANT_TYPE, &interface, &changed_properties, &invalidated_properties);

    gchar *entry_key;
    GVariant *entry_value;
    while (g_variant_iter_loop (changed_properties, "{sv}", &entry_key, &entry_value)){
        if(g_strcmp0("Metadata", entry_key)) continue;
        if(g_strcmp0(g_variant_get_type_string(parameters), PROPERTIES_VARIANT_TYPE) != 0) continue;

        GVariantDict *metadata_dict = g_variant_dict_new(entry_value);

        gchar *artist = NULL;
        const gchar *title = NULL;

        GVariant *artist_var = g_variant_dict_lookup_value(metadata_dict, "xesam:artist", G_VARIANT_TYPE_ARRAY);
        if(artist_var){
            artist = make_artist_string(artist_var);
        }

        GVariant *title_var = g_variant_dict_lookup_value(metadata_dict, "xesam:title", G_VARIANT_TYPE_STRING);
        if(title_var){
            title = g_variant_get_string(title_var, NULL);
        }

        gchar *artist_title = g_strdup_printf("%s - %s", artist, title);
        gchar *artist_title_normalized = utf8_to_ebu(artist_title);

        watcher->cb(artist_title_normalized, watcher->cb_data);

        g_free((gpointer) artist_title);
        g_free((gpointer) artist_title_normalized);
        g_free((gpointer) artist);

        g_variant_dict_unref(metadata_dict);
    }
}

static void watch(MPRISWatcher *watcher){
    if(watcher->conn) if(!g_dbus_connection_is_closed(watcher->conn)){
        if(watcher->sub_id > 0)
            g_dbus_connection_signal_unsubscribe(watcher->conn, watcher->sub_id);
        watcher->sub_id = g_dbus_connection_signal_subscribe(watcher->conn, NULL, NULL, "PropertiesChanged", OBJ_PATH, NULL, G_DBUS_SIGNAL_FLAGS_NONE, signal_cb, watcher, NULL);
    }
}

static void conn_cb (GObject *source_object, GAsyncResult *res, gpointer user_data){
    MPRISWatcher *watcher = user_data;
    g_object_unref(watcher->conn_cancellable);
    watcher->conn_cancellable = NULL;

    GError *error = NULL;
    watcher->conn = g_dbus_connection_new_for_address_finish(res, NULL);
    if(error){
        g_print("Error connecting to DBus: %s\n", error->message);
        g_error_free(error);
    }

    watch(watcher);
}

MPRISWatcher *MPRISWatcher_create(PRISWatcher_cb callback, gchar *path, gpointer cb_data){
    MPRISWatcher *watcher = g_new0(MPRISWatcher, 1);
    watcher->cb = callback;
    watcher->cb_data = cb_data;
    watcher->path = g_strdup(path);

    GError *error = NULL;
    gchar *addr = g_dbus_address_get_for_bus_sync(G_BUS_TYPE_SESSION, NULL, NULL);
    if(!addr){
        g_print("Error getting DBus address: %s\n", error->message);
        g_error_free(error);
        g_free(watcher->path);
        g_free(watcher);
        return NULL;
    }
    watcher->conn_cancellable = g_cancellable_new();
    g_dbus_connection_new_for_address(addr, G_DBUS_CONNECTION_FLAGS_AUTHENTICATION_CLIENT | G_DBUS_CONNECTION_FLAGS_MESSAGE_BUS_CONNECTION, NULL, watcher->conn_cancellable, conn_cb, watcher);
    g_free(addr);

    return watcher;
}

void MPRISWatcher_watch(MPRISWatcher *watcher, gchar *path){
    g_free(watcher->path);
    watcher->path = g_strdup(path);
    watch(watcher);
}

void MPRISWatcher_destroy(MPRISWatcher *watcher){
    g_free(watcher->path);
    if(watcher->conn_cancellable){
        g_cancellable_cancel(watcher->conn_cancellable);
        g_object_unref(watcher->conn_cancellable);
    }

    if(watcher->sub_id > 0)
        g_dbus_connection_signal_unsubscribe(watcher->conn, watcher->sub_id);

    if(!g_dbus_connection_is_closed(watcher->conn)) g_dbus_connection_close_sync(watcher->conn, NULL, NULL);
    g_object_unref(watcher->conn);

    g_free(watcher);
}
