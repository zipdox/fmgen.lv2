#include <math.h>
#include <stdlib.h>

#include "rrcosfilter.h"

static inline float *rrcosfilter(int taps, float period, float sample_rate){
    float *h = calloc(taps, sizeof(float));

    float t_delta = 1.0 / sample_rate;

    float sum = 0.0;
    for(int i = 0; i < taps; i++){
        float t = (i - (float)taps) * t_delta;
        if(t == 0.0){
            h[i] = 4.0 / M_PI;
        }else if(t == period / (4.0 * M_PI)){
            h[i] = 1.0 / sqrtf(2.0) * ((1.0 + 2.0 / M_PI) * sinf(M_PI / 4.0) + (1.0 - 2.0 / M_PI) * cosf(M_PI / 4.0));
        }else if(t == -period / (4.0 * M_PI)){
            h[i] = 1.0 / sqrtf(2.0) * ((1.0 + 2.0 / M_PI) * sinf(M_PI / 4.0) + (1.0 - 2.0 / M_PI) * cosf(M_PI / 4.0));
        }else{
            h[i] = 4.0 * t / period * cosf(M_PI * t * 2.0 / period) / (M_PI * t * (1.0 - (4.0 * t / period) * (4.0 * t / period)) / period);
        }
        sum += h[i];
    }

    // scale
    sum *= 2.0;
    for(int i = 0; i < taps; i++){
        h[i] /= sum;
    }

    return h;
}

static inline int ntaps(float sample_rate, float transition_width){
    int ntaps = (53.0 * sample_rate / (22.0 * transition_width));
    if (ntaps & 1) ntaps++;
    return ntaps;
}

RRCosFilter *RRCosFilter_create(float sample_rate, float period, float transition_width){
    RRCosFilter *filter = calloc(1, sizeof(RRCosFilter));
    filter->taps = ntaps(sample_rate, transition_width) / 2;
    filter->bufsize = filter->taps * 2;
    filter->h = rrcosfilter(filter->taps, period, sample_rate);
    filter->buf = calloc(filter->bufsize, sizeof(float));
    return filter;
}

float RRCosFilter_filter(RRCosFilter *filter, float value){
    if(++filter->head >= filter->bufsize) filter->head = 0;
    filter->buf[filter->head] = value;
    float filtered = 0.0;
    for(int i = 0, j = filter->head + 1, k = filter->head; i < filter->taps; i++, j++, k--){
        if(j >= filter->bufsize) j = 0;
        if(k < 0) k = filter->bufsize - 1;
        filtered += filter->h[i] * (filter->buf[j] + filter->buf[k]);
    }
    return filtered;
}

void RRCosFilter_destroy(RRCosFilter *filter){
    free(filter->h);
    free(filter->buf);
    free(filter);
}
