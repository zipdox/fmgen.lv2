typedef struct {
    float *h;
    int taps;
    int bufsize;
    float *buf;
    int head;
} RRCosFilter;

RRCosFilter *RRCosFilter_create(float sample_rate, float period, float transition_width);

float RRCosFilter_filter(RRCosFilter *filter, float value);

void RRCosFilter_destroy(RRCosFilter *filter);
